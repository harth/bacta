## bacta

This is a very early draft at an opinionated layer on top of mithril that manages:

- routing
- state management
- side effects
- more!

Please do not use this yet!  Very early days!

#### Example

[Live Example](https://flems.io/#0=N4IgzgxgTg9gNnEAuA2gBgDRoLoZAMwEs4BTMZFUAOwEMBbE5EAOgAsAXOxPCGK9kvyYgAvhmr1GSFgCtyPPgKHSAOlV5Uw7AAQAlGAFcBAYRh0ADtoC82gBTAa7dlDAiAlNYB8d4Gu3aAN0ISAHckOw8rbzpbAHJmWCMSAFpeC1iMbT9-bQB5ACMZEgh2ZgBrEgBPMFtHZzA3Zjoac1tsnO0ADy9tGNjzDK6PLKoOt2zxqnc1NQ0tPUMBAFkYABMDUmttX1HtVZJ8Gg32cP0k0ws1ERmqACEmw35bVZgIAwZ+Zny1ysyd-zOAnC9na2gAMjAAOaEKjhFCxAD0cChMMGgJIK3WpFwoIAYjAoJCYCdtPCEfgCUT2GjFhi1hsSDjdv4ABJmEhwxHMbk0QlgGlJTEMpn+dwYbJQQT7KC1JwuDz-HJzHRpcwQmj7VZbOouZg0EqEAIkC7mPiCdi2SYdUGS9gGKCjPp68wDTKg-yq9Wa7QAMh9vVsnpgGpIq0yOoa-lBVu01ymk1E4hAtAYTGYEDA8hAGiU7GEIlwIDgMLK5FQElT0johHYrCgxBAeHtiGkHHY5jASARCIMVHMZUh6bMCOrtfrcAAAgAmZhoZgAZhHNbrxCaMOYckbIHYlXMUnA0EI5jzYgr+9HK7gyS0kvoW+bTDbHa7Pb7A6HdCXY+I1+cJHo06zrOCI3v+dAblmO57kwkD1seiZnkw+T6uwNBrlQEH3lALYgE+nbdr2-aDmkCLISUNATnOc4AIzJJKQShMki6rIQWikShaHVhhm54FB+6wUeJ7YCIQA)

#### Quick Start

```js
import m from 'mithril'
import * as B from 'bacta'

const LoggedIn = B.maybe('LoggedIn')

B.mount( document.body, {
  Route: {
    Home: ['/', () => import('./components/home.js')],
    Login: ['/login', () => import('./components/login.js')]
  },
  initial(){
    return {
      user: LoggedIn.N()
    }
  },
  render({ activeComponent, ...attrs }){

    const Loaded = activeComponent()

    return Loaded 
      ? m(Loaded, attrs )
      : m('h1', 'Loading')
  }
})
```

## API

#### attrs

- setState
- getState
- link
- subLink
- route
- moduleCache
- Route
- activeModule
- activeComponent

#### B.mount

#### B.mount options

- Route
- initial
- services
- render
- default
- reducer
- location
- streamKey

#### B.component

#### B.types

#### B.createRoute

#### B.stream
