import m from 'mithril'
import stream from 'mithril/stream'
import * as superouter from 'superouter'
import * as A from 'attain'
import meiosis from './meiosis'
import * as CodeSplitting from './code-splitting.js'
import * as ActiveModule from './active-module.js'
import Z from './z'

const S = A.stream

export const SubRoute = ({ 
  getState
  , setState
  , SubRoute
  , Route
  , defaultRoute 
}) => {

  const newRoute = subRoute => route => ({
    type: route.type
    , tag: route.tag
    , value: {
      ...route.value
      ,args: decodeURI(SubRoute.toURL(subRoute).slice(1))
    }
  })
  
  const patch = (subRoute, options) => state => ({
    ...state, route: {
      ...newRoute(subRoute)(state.route)
      ,...options
    }
  })

  const get = A.run(
    getState,
    S.watch( x => x.route ),
    S.map(
      route => SubRoute.matchOr(
        () => defaultRoute, '/' + (
          // todo-james make it a standard part of superouter
          route.value
          && route.value.args
          ? decodeURI(route.value.args).replace(/\/$/, '')
          : ''
        )
      )
    )
  )

  const set = value => setState(patch(value))

  const link = (subRoute, options={}) => {
    return {
      href: Route.toURL(newRoute(subRoute)(getState().route))
      ,onclick(e){
        e.preventDefault()
        setState( patch(subRoute, options) )
      }
    }
  }

  return { get, set, link }
}

const ensureFn = x => typeof x == 'function' ? x : () => x

export function RouteMount(vnode) {
  const getAttrs = vnode.attrs.attrs

  const {
    getState: theirGetState,
    setState: theirSetState
  } = getAttrs()

  const getState = S.source(theirGetState)
  const setState = S.sink(theirSetState)

  return {
    onremove: () => {
      getState.end(true)
      setState.end(true)
    }
    ,view: ({ children, attrs: { attrs: getAttrs } }) => {
      const childAttrs = { ...getAttrs(), getState, setState }
      return children.map( f => f(childAttrs) )
    }
  }
}
const defaultRouteError = pathname =>
  'No route was found for url: '
    + pathname
    + '!  Please provide a defaultRoute.'

export const component = f => () => {
  return {
    view: ({ attrs }) => m(Inline, { ...attrs, view: f })
  }
}

export function Inline({ attrs }){
  const session = S.session()
  attrs.stream = session.of
  const view = attrs.view(attrs)
  return {
    onremove: () => session.end()
    ,view(attrs){
      return view(attrs)
    }
  }
}

export const cssVar = (varName, stream) =>
  inline(
    ({ stream: s }) => {

      const dom = s()

      S.merge([dom, stream()]).map(
        ([ dom, x ]) =>
          dom.parentElement.style.setProperty('--'+varName, x)
      )

      return () =>
        m('.span', {
          style: { display: 'none' }
          ,oncreate: (vnode) => dom(vnode.dom)
        })
    }
  )

export const renderStream = (stream) =>
  inline(
    ({ stream: s }) => {

      const dom = s()

      S.merge([dom, stream()]).map(
        ([ dom, x ]) =>
          m.render( dom, x )
      )

      return () =>
        m('.span', {
          oncreate: (vnode) => dom(vnode.dom)
        })
    }
  )

export const Hook = ({ attrs }) => {
  return {
    view: () => null
    ,...attrs
  }
}

export const hook = (key, effect) => 
  typeof key == 'function'
  ? m(Hook, { oncreate: key })
  : m(Hook, { key, oncreate: effect })


export const inline = (key, view) => {
  const hasKey = typeof key == 'string'

  /* eslint-disable no-param-reassign */
  view = hasKey ? view : key
  let attrs = 
      typeof view == 'function'
      ? { view }
      : view

  attrs.view = attrs.view || (() => () => null)
  /* eslint-enable no-param-reassign */
  return hasKey
    ? m(Inline, { key, ...attrs })
    : m(Inline, attrs)
}  

export const mount = (
  container, 
  {
    Route: theirRoute,
    initial: theirInitial,
    services: theirServices,
    default: theirDefault,
    reducer: theirReducer,
    location: theirLocation,
    streamKey: theirStreamKey,
    render: theirRender
  }
) => {
  
    // eslint-disable-next-line no-param-reassign
    theirRoute = theirRoute || {
      Home: ['/', null]
    }

    const render = 
      theirRender 
      || (
        ({ activeComponent, ...attrs }) => 
          activeComponent() 
          ? m(activeComponent(), attrs)
          : null
      )

    const Route = 
      superouter.type(
        'Route'
        , Object.fromEntries(
          Object.entries(theirRoute)
            .map( ([k,[urlPattern]]) => [k,urlPattern])
        )
      )

    const initial = (...args) => {
      const state = theirInitial ? theirInitial(...args) : {}

      if( !state.route ){
        state.route = defaultRoute
      }

      return state
    }
    
    const location = theirLocation 
      // eslint-disable-next-line no-undef
      || typeof window !== 'undefined' ? window.location : null

    const getPath = () => decodeURI(location.pathname)

    const streamKey = theirStreamKey || (({ route }) => route.tag)
    
    Route.fold = A.fold(Route.of)

    const download = 
      Route.fold(
        Object.fromEntries(
          Object.entries(theirRoute)
            .map( ([k,[,download]]) => 
              [k, ensureFn(download)])
        )
      )

    const failedMatchSentinel = {}

    const defaultRoute = 
      theirDefault
      ? theirDefault(Route)
      : Route.matchOr( () => failedMatchSentinel , '/')

    if( defaultRoute == failedMatchSentinel ){
      throw new Error(defaultRouteError('/'))
    }

    const toURL = Route.toURL
    const fromURL = route => Route.matchOr(() => defaultRoute, route)

    const reducer = theirReducer || ((x, f) => f(x))

    const Router = meiosis({
      toURL
      , fromURL
      , getPath
      , stream
    })

    const setState = stream()
    const active = stream()
    const moduleCache = stream({})

    const activeModule = active.map( x => x.module )

    const activeComponent = active.map( x => x.component )

    const getState = stream.scan(
      reducer
      , initial({ location })
      , setState
    )

    const link = route =>
      Router.link(setState)(route)

    const routeStream = 
      S.watch(x => x.route) (getState)

    const nullableActiveModule = 
      activeModule.map( A.getOr(null) )

    const nullableActiveComponent = 
      activeComponent.map( A.getOr(null) )

    const attrs = () => {
      
      const route = routeStream
      
      const activeModule = nullableActiveModule
      const activeComponent = nullableActiveComponent
      
      const OurSubRoute = (SubRouteType, defaultRoute) => SubRoute({
        getState
        , setState
        , Route
        , SubRoute: SubRouteType
        , defaultRoute
      })

      return {
        setState // stream
        ,getState // stream
        ,route // stream
        ,moduleCache // stream
        ,activeModule // stream
        ,activeComponent // stream
        // ,subLink: subLink() // fn
        ,link // fn
        ,Route // Type
        ,SubRoute: OurSubRoute
      }
    }
  
  function Main() {
    return {
      oncreate() {
        Router
          .start( getState )
          .map( setState )

        if( theirServices ) {
          theirServices(attrs())
        }

        CodeSplitting.ModuleCacheService (download) (getState, moduleCache)

        ActiveModule.Service({ getState, moduleCache })
          .map(active)
      

        stream.merge([getState, activeModule, moduleCache, activeComponent])
        .map(
          () => m.redraw()
        )
      }
      , view: () => 
          m(RouteMount,
            { key: streamKey(getState()), attrs },
            render
          )
    }
  }
  m.mount(container, function App() {
    return {
      view: () => m(Main, attrs)
    }
  })

  return {
    setState, getState, Route, route: routeStream
  }
}

// todo: Make superouter support this directly
export const createRoute = (...args) => {
  const type = superouter.type(...args)

  type.fold = A.fold(type.of)
  type.of = A.decorate(type.of)
  return type
}

const Oneline = (...args) =>
  String.raw(...args)
    .replace(/\n/g, '')
    .replace(/\s+/g, ' ')

const string = { Oneline }
export { string }
export const types = A
export { m }
export const Maybe = A.Either
export { Z }
export * from 'attain'