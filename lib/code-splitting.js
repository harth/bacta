import m from 'mithril'
import * as A from 'attain'

const Downloaded = A.either('Downloaded')

const ResolveService = download => getState => tell => {
  const route$ = getState.map( x => x.route )
  
  route$.map(
    route => Promise.resolve(download(route))
      .then( Downloaded.Y, Downloaded.N )
      .then( downloaded =>
        tell( downloaded )(route)
      )
  )
}

export const ModuleCacheService = download => (getState, moduleCache) => {
  
  const assoc = ({ route, module }) => moduleCache(
    { ...moduleCache(), [route.tag]: module },
  )

  // Actually download the code
  ResolveService(download)(getState)(
    Downloaded.fold({
      Y: module => route => {
        assoc({
          route, module
        })
        m.redraw()
      }
      , N: e => route =>
        // eslint-disable-next-line no-console, no-undef
        console.error('Error downloading code', e, route)
    })
  )
}
