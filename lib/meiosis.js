/* globals addEventListener, history, window */
import * as S from './stream'

const $ = {
  set: x => () => x
  ,compose: (...fns) => (...xs) => fns.reduceRight((p, n) => [n(...p)], xs)[0]

  ,select: f => o => {
    const r = [];
    f(x => {
      r.push(x);
      return x;
    })(o);
    return r;
  }
};

function Router({ toURL, fromURL, getPath, stream }) {

  const $route = f => o => Object.assign({}, o, { route: f(o.route) });

  const initial = o => $route($.set(fromURL(getPath())))(o);

  const link = update => (route, options={}) => ({
    href: toURL(route)
    ,onclick(e) {
      e.preventDefault();
      update($route(() => ({...route, ...options})));
    }
  });

  const startURL = url => {
    const popstates = stream();
    
    S.dropRepeats(url).map(url => {
      if (url+'' !== getPath()) {
        const search = 
          url.route.search
          || window.location.search
          
        if( url.route.replace ) {
          history.replaceState({}, "", url+search);  
        } else {
          history.pushState({}, "", url+search );
        }
      }
      return null;
    });

    addEventListener("popstate", () => popstates(getPath()));

    return popstates;
  };

  const start = model$ => {
    const url$ = model$.map(model =>
      [model]
        .flatMap($.select($route))
        .map(route => {
          const url = toURL(route)

          return {
            valueOf(){
              return url
            }
            ,route
          }
        })
        .shift()
    );

    return startURL(url$).map(url =>
      [url]
        .map(fromURL)
        .map(
          $.compose(
            $route,
            $.set
          )
        )
        .shift()
    );
  };

  return {
    link
    ,start
    ,initial
  };
}

export default Router;
