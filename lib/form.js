import m from 'mithril'
import * as stream from './stream.js'

export default function FormService() {
  const realInput = m.stream()

  const input = realInput.map( x => x)
  const errorInput = input.map( x => x )

  const touched = stream.dropRepeats(m.stream.scan(
    (theirP, target) => {
      const p = touched() || theirP
      if ( !p[target.name] )
        return { ...p, [target.name]: true }

      return p
    }
    , {}, realInput
  ))

  const notNull = x => x != null

  const vnode = m.stream()
  const originalValues = m.stream()

  const changed = originalValues.map( () => ({}))

  const values = stream.dropRepeats(m.stream.scan(
    (theirP, target) => {
      const p = values() || theirP
      const newValue = [
        [target.valueAsDate, notNull]
        ,[target.valueAsNumber, Number.isFinite]
        ,[target.value, notNull]
        ,[target.checked, notNull]
      ]
      .filter( ([x, f]) => f(x))
      .map( ([x]) => x )
      .find(Boolean)

      if (originalValues()[target.name] == newValue) {
        const without = { ...changed() }
        delete without[target.name]
        changed(without)
      } else {
        changed(
          { ...changed(), [target.name]: newValue }
        )
      }

      if (p[target.name] !== newValue )
        return { ...p, [target.name]: newValue }

      return p
    }, {}, input
  ))

  const validateInitialInputValues = ({ dom }) => {
    [].map.call(
      dom.querySelectorAll('[name]')
      , target => errorInput(target)
    )
  }

  originalValues.map(values)

  m.stream.merge([vnode, originalValues]).map(
    () => Promise.resolve(vnode())
      .then(validateInitialInputValues)
  )

  const errors = stream.dropRepeats(m.stream.scan(
    (theirP, target) => {
      const p = errors() || theirP
      if (p[target.name] !== target.validationMessage )
        return { ...p, [target.name]: target.validationMessage || null }

      return p
    }, {}, errorInput
  ))

  const allValid = errors.map(
    o => Object.values(o).every(
      s => s == null
    )
  )

  const someInvalid = allValid.map( x => !x )

  const unchanged = changed.map(
    o => Object.keys(o).length === 0
  )

  return {

    source: {
      originalValues
      ,values
    }

    ,sink: {
      values
      , touched
      , errors
      , allValid
      , someInvalid
      , unchanged
      , changed
    }

    ,children(f) {

      const attrs = {
        onsubmit: e => e.preventDefault()
        , oninput: e => realInput(e.target)
        , oncreate: vnode
      }

      return f({
        values
        , attrs
        , touched
        , errors
        , allValid
        , someInvalid
        , unchanged
        , changed
      })
    }
  }
}
