import * as A from 'attain'
import { of, dropRepeats, afterSilence } from './stream'

const Z = ({ 
  stream: theirStream
  , id
  , map=A.map
  , getWith=A.getWith 
}) => {

  const stream = of()
  const removed = of()
  stream.removed = removed
  let lastGet

  const over = f => {
    let out
    A.run(
      theirStream()
      
      , map(
        xs => xs.map(
          x =>
            id(x)
              ? out=f(x)
              : x
        )
      )
      , theirStream
    )
    stream(out)
    return out
  }

  stream.throttled =
    A.run(
      stream
      , dropRepeats
      , afterSilence(1000)
    )

  const set = x => over( () => x )

  const get = () =>
    A.run(
      theirStream()
      , getWith( null, xs => {
        const found =
          xs.find(id)
        // async redraw can mean a view is using Z.get()
        // to retrieve a list item that doesn't exist anymore
        // so we cache the last get
        lastGet = found || lastGet
        return lastGet
      } )
    )

  const remove = () => {
    const existing = get()

    A.run(
      theirStream()
      , map(
        xs => xs.flatMap(
          x =>
            id(x)
              ? []
              : [x]
        )
      )
      , theirStream
    )
    removed( existing )
  }

  return {
    get, set, over, remove, stream, removed
  }
}

export default Z