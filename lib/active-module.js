import stream from 'mithril/stream'
import * as S from './stream'
import { Either, run } from 'attain'

export const Service = ({ getState, moduleCache }) => {
  const route$ = S.watch( x => x.route ) (getState)

  const out = stream.merge([
    route$, moduleCache
  ])
  .map(
    ([ route, moduleCache ]) => {

      const module =
        Either.fromNullable(moduleCache[route.tag])
        
      const component =
        run(
          module,
          Either.chain(
            module => 
              module.Main
                ? Either.Y(module.Main)
              : module.default
                ? Either.Y(module.default)
              : module.view || typeof module == 'function'
                ? Either.Y(module)
                : Either.N()
          )
        )

      return { module, component }
    }
  )

  return out
}