import babel from 'rollup-plugin-babel'
import resolve from 'rollup-plugin-node-resolve'
import commonjs from 'rollup-plugin-commonjs'
// import {terser} from 'rollup-plugin-terser'

export default {
    input: './lib/index.js'
    ,plugins: [
        babel()
        ,resolve()
        ,commonjs()
        // ,terser()
    ]
    ,output: {
        globals: {
            mithril: "m"
            ,"mithril/stream": "m.stream"
        }
        ,file: './dist/bacta.min.js'
        ,format: 'umd'
        ,name: 'B'
        ,sourcemap: 'external'
    },

}
